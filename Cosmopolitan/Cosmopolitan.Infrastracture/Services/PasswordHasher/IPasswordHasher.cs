﻿namespace Cosmopolitan.Infrastracture.Services.PasswordHasher
{
    public interface IPasswordHasher
    {
        string GenerateIdentityV3Hash(string password);
        bool VerifyIdentityV3Hash(string password, string passwordHash);
    }
}