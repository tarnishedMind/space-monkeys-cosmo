﻿using Cosmopolitan.Infrastracture.Config;
using Cosmopolitan.Infrastracture.Mappings;
using NHibernate;
using NHibernate.Context;

namespace Cosmopolitan.Infrastracture.Services.Session
{
    public class WebSessionFactory
    {
        //private readonly ILogger<WebSessionContext> _logger;
        private readonly IConnectionString _connectionString;
        private ISessionFactory _sessionFactory;


        public WebSessionFactory(IConnectionString connectionString/*, ILogger<WebSessionContext> logger*/)
        {
            //_logger = logger;
            _connectionString = connectionString;
        }

        public ISessionFactory Init()
        {
            var types = typeof(TestMapping).Assembly.GetTypes();

            _sessionFactory = SessionFactory<WebSessionContext>.CreateSessionFactory(_connectionString.ConnectionName, types /*,_logger*/);

            return _sessionFactory;
        }
    }
}