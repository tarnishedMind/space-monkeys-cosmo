﻿using NHibernate;

namespace Cosmopolitan.Infrastracture.Services.Session
{
    public interface ISessionProvider
    {
        void OpenSession();
        ISession Session { get; }
        void CloseSession();
    }
}