﻿using Autofac;
using Cosmopolitan.Domains.Entities;
using Cosmopolitan.Domains.Repository.Base;
using Cosmopolitan.Infrastracture.Repository;
using Cosmopolitan.Infrastracture.Repository.Interfaces;
using Cosmopolitan.Infrastracture.Services.PasswordHasher;
using Cosmopolitan.Infrastracture.Translators;

namespace Cosmopolitan.Infrastracture.Config
{
    public class InfrastractureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CosmopolitanUserRepository>().As<ICosmopolitanUserRepository>().SingleInstance();
            builder.Register(c => c.Resolve<ICosmopolitanUserRepository>()).As<IRepository<CosmopolitanUser>>().SingleInstance();

            builder.RegisterType<FollowerRepository>().As<IFollowerRepository>().SingleInstance();
            builder.Register(c => c.Resolve<IFollowerRepository>()).As<IRepository<Follower>>().SingleInstance();

            builder.RegisterType<TranslatorFactory>().As<ITranslatorFactory>().SingleInstance()
                .OnActivating(c => c.Instance.Initialize());

            builder.RegisterType<PasswordHasher>().As<IPasswordHasher>().SingleInstance();
        }
    }
}