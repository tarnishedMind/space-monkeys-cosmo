﻿namespace Cosmopolitan.Infrastracture.Config
{
    public interface IConnectionString
    {
        string ConnectionName { get; set; }
    }
}