﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;

namespace Cosmopolitan.Infrastracture.Extensions
{
    public static class ComponentContextExtension
    {
        private static readonly Dictionary<Type, TypeConstructorInfo> CtorInfo;
        private static readonly object CtorInfoLock;

        static ComponentContextExtension()
        {
            CtorInfo = new Dictionary<Type, TypeConstructorInfo>();
            CtorInfoLock = new object();
        }

        public static T RuntimeResolve<T>(this IComponentContext kernel, params NamedParameter[] parameters)
            where T : class
        {
            return (T)kernel.RuntimeResolve(typeof(T), parameters);
        }

        public static object RuntimeResolve(this IComponentContext kernel, Type type, params NamedParameter[] parameters)
        {
            if (kernel.IsRegistered(type))
                return kernel.Resolve(type);

            bool cached;
            TypeConstructorInfo ctorInfo;
            lock (CtorInfoLock)
            {
                cached = CtorInfo.TryGetValue(type, out ctorInfo);
            }

            if (!cached)
            {
                ctorInfo = new TypeConstructorInfo(type);
                lock (CtorInfoLock)
                {
                    CtorInfo.Add(type, ctorInfo);
                }
            }

            var arguments = CreateParameters(kernel, ctorInfo, parameters);
            try
            {
                var result = ctorInfo.Ctor.Invoke(arguments.ToArray());
                return result;
            }
            catch (TargetInvocationException invocation)
            {
                throw new Exception($"Failed to construct {type.Name}", invocation);
            }
        }

        private static List<object> CreateParameters(IComponentContext kernel, TypeConstructorInfo ctorInfo, NamedParameter[] userParameters)
        {
            var arguments = new List<object>();

            foreach (var parameterInfo in ctorInfo.Parameters)
            {
                object parameterValue;
                var parameter = userParameters.FirstOrDefault(x => x.Name == parameterInfo.Name);

                if (parameter == null)
                {
                    parameterValue = kernel.ResolveOptional(parameterInfo.ParameterType);
                    if (parameterValue == null)
                    {
                        if (parameterInfo.IsOptional)
                            parameterValue = Type.Missing;
                        else
                            throw new Exception(
                                $"Failed to resolve parameter for parameter {parameterInfo.Name} of type {parameterInfo.ParameterType.Name} when creating {ctorInfo.Type.FullName}");
                    }
                }
                else
                    parameterValue = parameter.Value;

                arguments.Add(parameterValue);
            }

            return arguments;
        }
    }
}