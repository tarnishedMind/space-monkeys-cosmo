﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using AutoMapper;
using Cosmopolitan.Contacts.Dto.Account;
using Cosmopolitan.Contacts.Dto.User;
using Cosmopolitan.Domains.Entities;
using Cosmopolitan.Infrastracture.Extensions;
using Cosmopolitan.Infrastracture.Translators.ObjectMapper.DtoToModel;

namespace Cosmopolitan.Infrastracture.Translators
{
    public class TranslatorFactory : ITranslatorFactory
    {
        private readonly IComponentContext _componentContext;
        protected IMapper Mapper;

        protected MapperConfiguration MapperConfiguration;

        private readonly List<TranslatorData> _translators;

        public TranslatorFactory(IComponentContext componentContext)
        {
            _componentContext = componentContext;
            _translators = new List<TranslatorData>();
        }

        public void Initialize()
        {
            MapperConfiguration = new MapperConfiguration(Configure);
            Mapper = MapperConfiguration.CreateMapper();
        }

        private void Configure(IMapperConfigurationExpression configurationExpression)
        {
            //AddTranslator
            AddTranslator<UserLoginDto, CosmopolitanUser, UserLoginDtoToCosmopolitanUserTranslator>(configurationExpression);
            AddTranslator<FollowerDto, Follower, FollowerDtoToFollowerTranslator>(configurationExpression);
        }

        public void AddTranslator<TSource, TDestination, TTranslatorType>(IMapperConfigurationExpression expression) where TTranslatorType : class, ITranslator<TSource, TDestination>
        {
            var translator = _componentContext.RuntimeResolve<TTranslatorType>(new NamedParameter("mapper", new Lazy<IMapper>(() => Mapper)), new NamedParameter("expression", expression));
            translator.Configure();
            _translators.Add(new TranslatorData
            {
                Destination = typeof(TDestination),
                Source = typeof(TSource),
                Translator = translator
            });
        }

        public ITranslator<TSource, TDestination> GetTranslator<TSource, TDestination>()
        {
            return (ITranslator<TSource, TDestination>)_translators.FirstOrDefault(c =>
                    c.Destination == typeof(TDestination) &&
                    c.Source == typeof(TSource))
                ?.Translator;
        }
    }
}