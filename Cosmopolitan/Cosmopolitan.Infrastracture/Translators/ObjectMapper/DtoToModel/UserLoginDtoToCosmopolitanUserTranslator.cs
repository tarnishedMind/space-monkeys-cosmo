﻿using System;
using AutoMapper;
using Cosmopolitan.Contacts.Dto.Account;
using Cosmopolitan.Domains.Entities;
using Cosmopolitan.Infrastracture.Services.PasswordHasher;

namespace Cosmopolitan.Infrastracture.Translators.ObjectMapper.DtoToModel
{
    public class UserLoginDtoToCosmopolitanUserTranslator : Translator<UserLoginDto, CosmopolitanUser>
    {
        private readonly IPasswordHasher _passwordHasher;
        private string _password;
        public UserLoginDtoToCosmopolitanUserTranslator(Lazy<IMapper> mapper, IMapperConfigurationExpression expression,
        IPasswordHasher passwordHasher) : base(mapper, expression)
        {
            _passwordHasher = passwordHasher;
        }

        public override void Configure()
        {
            Mapping
                .BeforeMap((dto, user) => { _password = _passwordHasher.GenerateIdentityV3Hash(dto.Password); })
                .ForMember(c => c.Login, k => k.MapFrom(c => c.Login))
                .ForMember(c => c.PasswordHash, k => k.MapFrom(c => _password));
        }
    }
}