﻿using System;
using AutoMapper;
using Cosmopolitan.Contacts.Dto.User;
using Cosmopolitan.Domains.Entities;

namespace Cosmopolitan.Infrastracture.Translators.ObjectMapper.DtoToModel
{
    public class FollowerDtoToFollowerTranslator : Translator<FollowerDto, Follower>
    {
        public FollowerDtoToFollowerTranslator(Lazy<IMapper> mapper, IMapperConfigurationExpression expression) : base(mapper, expression)
        {
        }

        public override void Configure()
        {
            Mapping
                .ForMember(c => c.IdUser, k => k.MapFrom(c => c.IdUser));
        }
    }
}