﻿using AutoMapper;

namespace Cosmopolitan.Infrastracture.Translators
{
    public interface ITranslatorFactory
    {
        void Initialize();
        void AddTranslator<TSource, TDestination, TTranslatorType>(IMapperConfigurationExpression expression) where TTranslatorType : class, ITranslator<TSource, TDestination>;
        ITranslator<TSource, TDestination> GetTranslator<TSource, TDestination>();
    }
}