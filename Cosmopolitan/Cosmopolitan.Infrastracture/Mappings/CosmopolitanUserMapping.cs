﻿using Cosmopolitan.Domains.Entities;
using Cosmopolitan.Infrastracture.Mappings.Base;
using NHibernate.Mapping.ByCode;

namespace Cosmopolitan.Infrastracture.Mappings
{
    public class CosmopolitanUserMapping : EntityMapping<CosmopolitanUser>
    {
        public CosmopolitanUserMapping()
        {
            Property(c => c.Login);
            Property(c => c.PasswordHash);
            Bag(x => x.IdPosts, k =>
            {
                k.Cascade(Cascade.All);
            },
                map => map.OneToMany());
        }
    }
}