﻿using Cosmopolitan.Domains.Entities;
using Cosmopolitan.Infrastracture.Mappings.Base;

namespace Cosmopolitan.Infrastracture.Mappings
{
    public class FollowerMapping : EntityMapping<Follower>
    {
        public FollowerMapping()
        {
            Property(c => c.IdUser);
        }
    }
}