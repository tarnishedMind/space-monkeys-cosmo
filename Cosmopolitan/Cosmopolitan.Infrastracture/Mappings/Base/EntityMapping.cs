﻿using Cosmopolitan.Domains.Entities.Base;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Cosmopolitan.Infrastracture.Mappings.Base
{
    public class EntityMapping<T> : ClassMapping<T> where T : Entity
    {
        public EntityMapping()
        {
            Id(x => x.Id, m => m.Generator(new GuidGeneratorDef()));
            Property(x => x.IsDeleted);
            Property(x => x.Timestamp);
        }
    }
}