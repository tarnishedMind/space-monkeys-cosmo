﻿namespace Cosmopolitan.Infrastracture.Api.Vk
{
    public class VkRequestUrlBuilder
    {
        private static string _url;

        private VkRequestUrlBuilder()
        {
            
        }

        public static VkRequestUrlBuilder CreateBuilder()
        {
            _url = "https://api.vk.com/method/";
            return new VkRequestUrlBuilder();
        }

        public VkRequestUrlBuilder AddDefault(string entity, string method)
        {
            _url += $"{entity}.{method}?v=5.73&access_token={ApiHelper.VkToken}";
            return this;
        }
        public VkRequestUrlBuilder AddSort(string sort)
        {
            if(sort != "")
                _url += $"&sort={sort}";
            return this;
        }

        public VkRequestUrlBuilder AddCountry(string country)
        {
            if(country != "")
                _url += $"&country={country}";
            return this;
        }

        public VkRequestUrlBuilder AddOwnerId(string ownerId)
        {
            if (ownerId != "")
                _url += $"&owner_id={ownerId}";
            return this;
        }

        public VkRequestUrlBuilder AddFilter(string filter)
        {
            if (filter != "")
                _url += $"&filter={filter}";
            return this;
        }

        public VkRequestUrlBuilder AddQ(string q)
        {
            if (q != "")
                _url += $"&q={q}";
            return this;
        }

        public VkRequestUrlBuilder AddFields(string fields)
        {
            if (fields != "")
                _url += $"&fields={fields}";
            return this;
        }

        public string Build()
        {
            return _url;
        }
    }
}