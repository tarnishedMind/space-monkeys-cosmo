﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cosmopolitan.Contacts.Dto.Vk;
using Cosmopolitan.Domains.Enums.Api;
using NHibernate.Mapping;

namespace Cosmopolitan.Infrastracture.Api.Vk
{
    public class VkService
    {
        private readonly List<ApiEntityServiceData> _apiEntityServiceDatas;
        public VkService()
        {
            _apiEntityServiceDatas = new List<ApiEntityServiceData>();
        }

        public void Initialize()
        {
            AddData<VkUserDto, VkUserDto>(EVkEntityEnum.Users, EVkMethodsEnum.Search);
            AddData<VkWallUserDto, VkWallUserDto>(EVkEntityEnum.Wall, EVkMethodsEnum.Get);
        }

        public void AddData<TEntity, TDto>(EVkEntityEnum vkEntityEnum, EVkMethodsEnum vkMethodsEnum)
        {
            _apiEntityServiceDatas.Add(
                new ApiEntityServiceData
                {
                    EntityEnum = vkEntityEnum,
                    Dto = typeof(TDto),
                    Entity = typeof(TEntity),
                    MethodsEnum = vkMethodsEnum
                });
        }

        public Type GetEntityType(string @enum)
        {
            var entityType = GetEEntityType(@enum);
            return _apiEntityServiceDatas.FirstOrDefault(c => c.EntityEnum == entityType)?.Entity;
        }

        public Type GetDtoType(string @enum)
        {
            var entityType = GetEEntityType(@enum);
            return _apiEntityServiceDatas.FirstOrDefault(c => c.EntityEnum == entityType)?.Dto;
        }

        public string GetMethodEntity(string @enum)
        {
            var entityType = GetEEntityType(@enum);
            return _apiEntityServiceDatas.FirstOrDefault(c => c.EntityEnum == entityType)?.MethodsEnum.ToString().ToLower();
        }

        private static EVkEntityEnum GetEEntityType(string entityEnum)
        {
            if (!Enum.TryParse(entityEnum, true, out EVkEntityEnum entityType))
            {
                //throw new EntityTypeNotFoundException($"EntityEnum '{entityEnum}' not found");
            }

            return entityType;
        }
    }
}