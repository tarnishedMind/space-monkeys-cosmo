﻿using System;
using Cosmopolitan.Domains.Enums.Api;

namespace Cosmopolitan.Infrastracture.Api
{
    public class ApiEntityServiceData
    {
        public EVkEntityEnum EntityEnum;
        public EVkMethodsEnum MethodsEnum;
        public Type Entity;
        public Type Dto;
    }
}