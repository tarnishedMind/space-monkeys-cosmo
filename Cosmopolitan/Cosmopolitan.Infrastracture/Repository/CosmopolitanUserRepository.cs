﻿using System.Linq;
using Cosmopolitan.Domains.Entities;
using Cosmopolitan.Infrastracture.Repository.Base;
using Cosmopolitan.Infrastracture.Repository.Interfaces;
using Cosmopolitan.Infrastracture.Services.Session;

namespace Cosmopolitan.Infrastracture.Repository
{
    public class CosmopolitanUserRepository : RepositoryBase<CosmopolitanUser>, ICosmopolitanUserRepository
    {
        public CosmopolitanUserRepository(ISessionProvider sessionProvider) : base(sessionProvider)
        {
        }

        public CosmopolitanUser GetUserByLogin(string login)
        {
            return Session.Query<CosmopolitanUser>().FirstOrDefault(c => c.Login == login);
        }
    }
}