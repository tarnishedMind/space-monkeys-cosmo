﻿using Cosmopolitan.Domains.Entities;
using Cosmopolitan.Infrastracture.Repository.Base;
using Cosmopolitan.Infrastracture.Repository.Interfaces;
using Cosmopolitan.Infrastracture.Services.Session;

namespace Cosmopolitan.Infrastracture.Repository
{
    public class FollowerRepository : RepositoryBase<Follower>, IFollowerRepository
    {
        public FollowerRepository(ISessionProvider sessionProvider) : base(sessionProvider)
        {
        }
    }
}