﻿using System;
using System.Linq;
using Cosmopolitan.Domains.Entities.Base;
using Cosmopolitan.Domains.Repository.Base;
using Cosmopolitan.Infrastracture.Services.Session;
using NHibernate;

namespace Cosmopolitan.Infrastracture.Repository.Base
{
    public class RepositoryBase<T> : IRepository<T> where T : Entity
    {
        private readonly ISessionProvider _sessionProvider;
        protected ISession Session => _sessionProvider.Session;

        public RepositoryBase(ISessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }
        public void Delete(T entity)
        {
            Session.Clear();
            Session.Delete(entity);
            Session.Flush();
        }

        public T GetById(Guid id)
        {
            return Session.Get<T>(id);
        }

        public IQueryable<T> List()
        {
            return Session.Query<T>();
        }

        public void Save(T entity)
        {
            Session.Clear();
            Session.SaveOrUpdate(entity);
            Session.Flush();
        }
    }
}