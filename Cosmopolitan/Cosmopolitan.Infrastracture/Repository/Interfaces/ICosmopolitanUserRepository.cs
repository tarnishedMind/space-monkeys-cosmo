﻿using Cosmopolitan.Domains.Entities;
using Cosmopolitan.Domains.Repository.Base;

namespace Cosmopolitan.Infrastracture.Repository.Interfaces
{
    public interface ICosmopolitanUserRepository : IRepository<CosmopolitanUser>
    {
        CosmopolitanUser GetUserByLogin(string login);
    }
}