﻿using Autofac;
using Cosmopolitan.Config;
using Cosmopolitan.Infrastracture.Config;

namespace Cosmopolitan.Bootstrap
{
    public class BackendContainer : ContainerBuilder
    {
        public BackendContainer()
        {
            this.RegisterModule<AutofacModule>();
            this.RegisterModule<InfrastractureModule>();
        }
    }
}