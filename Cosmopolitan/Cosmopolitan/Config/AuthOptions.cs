﻿using System;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Cosmopolitan.Config
{
    public class AuthOptions
    {
        public const string ISSUER = "MyAuthServer"; // издатель токена
        public const string AUDIENCE = "http://localhost:5000/"; // потребитель токена
        const string KEY = "mysupersecret_secretkey!123";   // ключ для шифрации
        public const int LIFETIME = 24; // время жизни токена - 24 часа
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }

        public static DateTime GetExpires()
        {
            var now = DateTime.UtcNow;
            return now.Add(TimeSpan.FromHours(LIFETIME));
        }
    }
}