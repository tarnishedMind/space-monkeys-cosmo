﻿using Autofac;
using Cosmopolitan.Infrastracture.Api.Vk;
using Cosmopolitan.Infrastracture.Config;
using Cosmopolitan.Infrastracture.Services.Session;
using Cosmopolitan.Services.Identity;
using Cosmopolitan.Services.Session;
using Cosmopolitan.Services.Token;

namespace Cosmopolitan.Config
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SessionProvider>().As<ISessionProvider>().SingleInstance();

            builder.RegisterType<ConnectionString>().As<IConnectionString>().SingleInstance();

            builder.RegisterType<IdentityProvider>().As<IIdentityProvider>().SingleInstance();

            builder.RegisterType<TokenService>().As<ITokenService>().SingleInstance();

            builder.RegisterType<WebSessionFactory>().AsSelf().SingleInstance();

            builder.RegisterType<VkService>().AsSelf().SingleInstance()
                .OnActivating(c => c.Instance.Initialize());
        }
    }
}