﻿using Autofac;
using Cosmopolitan.Contacts.Dto.Account;
using Cosmopolitan.Controllers.Base;
using Cosmopolitan.Controllers.Base.Commands.Account;
using Cosmopolitan.Controllers.Base.Commands.Base;
using Cosmopolitan.Infrastracture.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Cosmopolitan.Controllers.Account
{
    [Route("[controller]")]
    public class AccountController : CommandController
    {
        private readonly IComponentContext _componentContext;

        public AccountController(IComponentContext componentContext)
        {
            _componentContext = componentContext;
        }

        [HttpPost]
        [Route("login")]
        [IgnoreAntiforgeryToken]
        public IActionResult Login([FromBody] JObject user)
        {
            var commandType = typeof(LoginAccountCommand);
            var activationCommand = (ICommand<TokenDto>)_componentContext.RuntimeResolve(commandType, new NamedParameter("data", user));
            return ExecuteData(activationCommand);
        }

        [HttpPost]
        [Route("register")]
        public IActionResult Register([FromBody] JObject user)
        {
            var commandType = typeof(RegisterAccountCommand);
            var activationCommand = (ICommand)_componentContext.RuntimeResolve(commandType, new NamedParameter("data", user));
            return Execute(activationCommand);
        }
    }
}