﻿using System.Threading.Tasks;
using Autofac;
using Cosmopolitan.Controllers.Base;
using Cosmopolitan.Controllers.Base.Commands.VkApiCommands;
using Cosmopolitan.Infrastracture.Api.Vk;
using Cosmopolitan.Infrastracture.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Cosmopolitan.Controllers.Api.Vk
{
    [Route("[controller]")]
    public class VkApiController : CommandController
    {
        private readonly VkService _vkService;
        private readonly IComponentContext _componentContext;

        public VkApiController(VkService vkService, IComponentContext componentContext)
        {
            _vkService = vkService;
            _componentContext = componentContext;
        }

        [HttpGet]
        [Route("{entity}/item")]
        public async Task<IActionResult> GetUsers(string entity, string country = "", string sort = "", string ownerId = "", string filter = "", string q = "", string fields = "")
        {
            var entityType = _vkService.GetEntityType(entity);

            var entityMethod = _vkService.GetMethodEntity(entity);

            var commandType = typeof(GetVkApiCommand<>);
            commandType = commandType.MakeGenericType(entityType);

            var activationCommand = _componentContext.RuntimeResolve(commandType, new NamedParameter("entity", entity), new NamedParameter("entityMethod", entityMethod),
                new NamedParameter("country", country), new NamedParameter("sort", sort), new NamedParameter("ownerId", ownerId),
                new NamedParameter("filter", filter), new NamedParameter("q", q), new NamedParameter("fields", fields));
            var info = GetType().GetMethod("ExecuteDataAsync").MakeGenericMethod(entityType);
            return await (Task<IActionResult>) info.Invoke(this, new[] {activationCommand});
        }
    }
}