﻿using System.Collections.Generic;
using Autofac;
using Cosmopolitan.Controllers.Base;
using Cosmopolitan.Controllers.Base.Commands.Base;
using Cosmopolitan.Controllers.Base.Commands.User;
using Cosmopolitan.Domains.Entities;
using Cosmopolitan.Infrastracture.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json.Linq;

namespace Cosmopolitan.Controllers.User
{
    [Route("[controller]")]
    [Authorize]
    public class UserController : CommandController
    {
        private readonly IComponentContext _componentContext;

        public UserController(IComponentContext componentContext)
        {
            _componentContext = componentContext;
        }

        [HttpGet]
        [Route("getFollowers")]
        public IActionResult GetFollowers()
        {
            var commandType = typeof(GetFollowersUser);
            var activationCommand = (ICommand<ICollection<Follower>>)_componentContext.RuntimeResolve(commandType, new NamedParameter("context", HttpContext));
            return ExecuteData(activationCommand);
        }

        [HttpPost]
        [Route("addFollower")]
        public IActionResult AddFollower([FromBody] JObject data)
        {
            var commandType = typeof(AddFollowerCommand);
            var activationCommand = (ICommand)_componentContext.RuntimeResolve(commandType, new NamedParameter("data", data), new NamedParameter("context", HttpContext));
            return Execute(activationCommand);
        }
    }
}