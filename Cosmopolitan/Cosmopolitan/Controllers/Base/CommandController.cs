﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Cosmopolitan.Controllers.Base.Commands.Base;
using Microsoft.AspNetCore.Mvc;

namespace Cosmopolitan.Controllers.Base
{
    public class CommandController : ControllerBase
    {
        public CommandController()
        {
            
        }

        public IActionResult Execute(ICommand command)
        {
            JsonResult res;
            try
            {
                var time = new Stopwatch();
                time.Start();
                var result = command.Run();
                res = new JsonResult(result)
                {
                    StatusCode = 200
                };
                time.Stop();

                //_logger.LogInformation($"Excecution time: {time.Elapsed}");
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, $"An error occurred while executing the command. Error stack: {ex.Message}");
                var result = new CommandResult
                {
                    Success = false,
                    Error = new Error
                    {
                        Code = 500,
                        Message = ex.Message + ex.StackTrace
                    }
                };

                res = new JsonResult(result)
                {
                    StatusCode = 500
                };
            }

            return res;
        }

        public IActionResult ExecuteData<TResult>(ICommand<TResult> command)
        {
            JsonResult res;
            try
            {
                var time = new Stopwatch();
                time.Start();
                var result = command.Run();
                res = new JsonResult(result)
                {
                    StatusCode = 200
                };

                time.Stop();

                //_logger.LogInformation($"Excecution time: {time.Elapsed}");
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, $"An error occurred while executing the command. Error stack: {ex.Message}");
                var result = new CommandResult
                {
                    Success = false,
                    Error = new Error
                    {
                        Code = 9999,
                        Message = ex.Message + ex.StackTrace
                    }
                };

                res = new JsonResult(result)
                {
                    StatusCode = 500
                };
            }

            return res;
        }

        public async Task<IActionResult> ExecuteDataAsync<TResult>(ICommandAsync<TResult> command)
        {
            JsonResult res;
            try
            {
                var time = new Stopwatch();
                time.Start();
                var result = await command.Run();
                res = new JsonResult(result)
                {
                    StatusCode = 200
                };

                time.Stop();

                //_logger.LogInformation($"Excecution time: {time.Elapsed}");
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, $"An error occurred while executing the command. Error stack: {ex.Message}");
                var result = new CommandResult
                {
                    Success = false,
                    Error = new Error
                    {
                        Code = 9999,
                        Message = ex.Message + ex.StackTrace
                    }
                };

                res = new JsonResult(result)
                {
                    StatusCode = 500
                };
            }

            return res;
        }
    }
}