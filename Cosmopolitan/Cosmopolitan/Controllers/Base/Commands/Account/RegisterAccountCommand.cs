﻿using Cosmopolitan.Contacts.Dto.Account;
using Cosmopolitan.Controllers.Base.Commands.Base;
using Cosmopolitan.Domains.Entities;
using Cosmopolitan.Infrastracture.Extensions;
using Cosmopolitan.Infrastracture.Repository.Interfaces;
using Cosmopolitan.Infrastracture.Translators;
using Newtonsoft.Json.Linq;

namespace Cosmopolitan.Controllers.Base.Commands.Account
{
    public class RegisterAccountCommand : ICommand
    {
        private readonly ICosmopolitanUserRepository _repository;
        private readonly ITranslatorFactory _translatorFactory;
        private readonly JObject _data;

        public RegisterAccountCommand(ICosmopolitanUserRepository repository, ITranslatorFactory translatorFactory, JObject data)
        {
            _repository = repository;
            _translatorFactory = translatorFactory;
            _data = data;
        }
        public ICommandResult Run()
        {
            var data = _data.ToObject<UserLoginDto>();

            var translator = _translatorFactory.GetTranslator<UserLoginDto, CosmopolitanUser>();
            var entity = translator.Translate(data);
            var user = _repository.GetUserByLogin(entity.Login);
            if (user != null)
            {
                return new CommandResult
                {
                    Success = false,
                    Error = new Error
                    {
                        Code = 2000,
                        Message = "User is already registered"
                    }
                };
            }

            entity.MarkAsNew();
            _repository.Save(entity);

            return new CommandResult
            {
                Success = true
            };
        }
    }
}