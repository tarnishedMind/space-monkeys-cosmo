﻿using Cosmopolitan.Contacts.Dto.Account;
using Cosmopolitan.Controllers.Base.Commands.Base;
using Cosmopolitan.Infrastracture.Repository.Interfaces;
using Cosmopolitan.Infrastracture.Services.PasswordHasher;
using Cosmopolitan.Services.Identity;
using Cosmopolitan.Services.Token;
using Newtonsoft.Json.Linq;

namespace Cosmopolitan.Controllers.Base.Commands.Account
{
    public class LoginAccountCommand : ICommand<TokenDto>
    {
        private readonly ICosmopolitanUserRepository _repository;
        private readonly IIdentityProvider _identityProvider;
        private readonly IPasswordHasher _passwordHasher;
        private readonly ITokenService _tokenService;
        private readonly JObject _data;

        public LoginAccountCommand(ICosmopolitanUserRepository repository, IIdentityProvider identityProvider, IPasswordHasher passwordHasher, ITokenService tokenService, JObject data)
        {
            _repository = repository;
            _identityProvider = identityProvider;
            _passwordHasher = passwordHasher;
            _tokenService = tokenService;
            _data = data;
        }
        public ICommandResult<TokenDto> Run()
        {
            var data = _data.ToObject<UserLoginDto>();
            var user = _repository.GetUserByLogin(data.Login);

            if (user == null || !_passwordHasher.VerifyIdentityV3Hash(data.Password, user.PasswordHash))
            {
                return new CommandResult<TokenDto>
                {
                    Success = false,
                    Error = new Error
                    {
                        Code = 1000,
                        Message = "Incorrect login or password"
                    }
                };
            }
            var identity = _identityProvider.GetIdentity(user.Login);

            var accessToken = _tokenService.GenerateAccessToken(identity);

            return new CommandResult<TokenDto>
            {
                Success = true,
                Result = new TokenDto
                {
                    access_token = accessToken
                }
            };
        }
    }
}