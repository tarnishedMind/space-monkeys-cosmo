﻿using System.Threading.Tasks;

namespace Cosmopolitan.Controllers.Base.Commands.Base
{
    public interface ICommand
    {
        ICommandResult Run();
    }

    public interface ICommand<TResult>
    {
        ICommandResult<TResult> Run();
    }

    public interface ICommandAsync<TResult>
    {
        Task<ICommandResult<TResult>> Run();
    }
}