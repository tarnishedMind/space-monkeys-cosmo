﻿namespace Cosmopolitan.Controllers.Base.Commands.Base
{
    public class CommandResult : ICommandResult
    {
        public bool Success { get; set; }
        public Error Error { get; set; }
    }

    public class CommandResult<TResult> : ICommandResult<TResult>
    {
        public bool Success { get; set; }
        public Error Error { get; set; }
        public TResult Result { get; set; }
    }

    public class Error
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}