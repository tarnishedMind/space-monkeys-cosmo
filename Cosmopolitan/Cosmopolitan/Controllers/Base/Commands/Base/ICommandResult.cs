﻿namespace Cosmopolitan.Controllers.Base.Commands.Base
{
    public interface ICommandResult
    {
        bool Success { get; set; }
        Error Error { get; set; }
    }

    public interface ICommandResult<TResult> : ICommandResult
    {
        TResult Result { get; set; }
    }
}