﻿using System.Collections.Generic;
using Cosmopolitan.Controllers.Base.Commands.Base;
using Cosmopolitan.Domains.Entities;
using Cosmopolitan.Infrastracture.Repository.Interfaces;
using Cosmopolitan.Services.Token;
using Microsoft.AspNetCore.Http;

namespace Cosmopolitan.Controllers.Base.Commands.User
{
    public class GetFollowersUser : ICommand<ICollection<Follower>>
    {
        private readonly ICosmopolitanUserRepository _repository;
        private readonly ITokenService _tokenService;
        private readonly HttpContext _context;

        public GetFollowersUser(ICosmopolitanUserRepository repository, ITokenService tokenService, HttpContext context)
        {
            _repository = repository;
            _tokenService = tokenService;
            _context = context;
        }
        public ICommandResult<ICollection<Follower>> Run()
        {
            var token = _context.Request.Headers["Authorization"].ToString();
            token = token.Substring(7);
            var name = _tokenService.GetNameFromToken(token);
            var user = _repository.GetUserByLogin(name);
            return new CommandResult<ICollection<Follower>>
            {
                Success = true,
                Result = user.IdPosts
            };
        }
    }
}