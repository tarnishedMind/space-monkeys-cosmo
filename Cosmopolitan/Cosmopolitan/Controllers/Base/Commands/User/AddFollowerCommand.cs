﻿using Cosmopolitan.Contacts.Dto.User;
using Cosmopolitan.Controllers.Base.Commands.Base;
using Cosmopolitan.Domains.Entities;
using Cosmopolitan.Domains.Repository.Base;
using Cosmopolitan.Infrastracture.Extensions;
using Cosmopolitan.Infrastracture.Repository.Interfaces;
using Cosmopolitan.Infrastracture.Translators;
using Cosmopolitan.Services.Token;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;

namespace Cosmopolitan.Controllers.Base.Commands.User
{
    public class AddFollowerCommand : ICommand
    {
        private readonly ICosmopolitanUserRepository _repository;
        private readonly ITokenService _tokenService;
        private readonly ITranslatorFactory _translatorFactory;
        private readonly JObject _data;
        private readonly HttpContext _context;

        public AddFollowerCommand(ICosmopolitanUserRepository repository, ITokenService tokenService, ITranslatorFactory translatorFactory, JObject data, HttpContext context)
        {
            _repository = repository;
            _tokenService = tokenService;
            _translatorFactory = translatorFactory;
            _data = data;
            _context = context;
        }
        public ICommandResult Run()
        {
            var token = _context.Request.Headers["Authorization"].ToString();
            token = token.Substring(7);
            var data = _data.ToObject<AddFollowerDto>();
            var name = _tokenService.GetNameFromToken(token);
            var user = _repository.GetUserByLogin(name);
            var translator = _translatorFactory.GetTranslator<FollowerDto, Follower>();
            user.IdPosts = translator.TranslateCollection(data.followers);
            user.MarkAsModified();
            _repository.Save(user);
            return new CommandResult
            {
                Success = true
            };
        }
    }
}