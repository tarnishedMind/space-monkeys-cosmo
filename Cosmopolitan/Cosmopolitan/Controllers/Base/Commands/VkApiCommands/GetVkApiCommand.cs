﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Cosmopolitan.Controllers.Base.Commands.Base;
using Cosmopolitan.Infrastracture.Api;
using Cosmopolitan.Infrastracture.Api.Vk;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;

namespace Cosmopolitan.Controllers.Base.Commands.VkApiCommands
{
    public class GetVkApiCommand<TDto> : ICommandAsync<TDto>
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IConfiguration _configuration;
        private readonly string _entity;
        private readonly string _entityMethod;
        private readonly string _country;
        private readonly string _sort;
        private readonly string _ownerId;
        private readonly string _filter;
        private readonly string _q;
        private readonly string _fields;

        public GetVkApiCommand(IHttpClientFactory clientFactory, IConfiguration configuration, string entity, string entityMethod, string country, string sort, string ownerId, string filter, string q, string fields)
        {
            _clientFactory = clientFactory;
            _configuration = configuration;
            _entity = entity;
            _entityMethod = entityMethod;
            _country = country;
            _sort = sort;
            _ownerId = ownerId;
            _filter = filter;
            _q = q;
            _fields = fields;
        }

        public async Task<ICommandResult<TDto>> Run()
        {
            var url = VkRequestUrlBuilder.CreateBuilder()
                .AddDefault(_entity, _entityMethod)
                .AddSort(_sort)
                .AddCountry(_country)
                .AddOwnerId(_ownerId)
                .AddFilter(_filter)
                .AddFields(_fields)
                .AddQ(_q)
                .Build();

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
                return new CommandResult<TDto>
                {
                    Success = false
                };

            var content = response.Content.ReadAsStringAsync().Result;
            var result = JsonConvert.DeserializeObject<TDto>(content);

            //run_cmd("Hello");

            return new CommandResult<TDto>
            {
                Success = true,
                Result = result
            };

        }

        private void run_cmd(string args)
        {
            ProcessStartInfo start = new ProcessStartInfo();
            var cmd = _configuration.GetSection("Python").GetSection("PythonFileExe").Value;
            var arg = _configuration.GetSection("Python").GetSection("ExecuteFile").Value;
            start.FileName = cmd;//cmd is full path to python.exe
            start.Arguments = arg + " " + args;//args is path to .py file and any cmd line args
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Console.Write(result);
                }
            }
        }
    }
}