﻿using System.Security.Claims;

namespace Cosmopolitan.Services.Token
{
    public interface ITokenService
    {

        string GenerateAccessToken(ClaimsIdentity identity);
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
        string GetNameFromToken(string token);
    }
}