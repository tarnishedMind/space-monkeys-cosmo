﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Cosmopolitan.Services.Identity
{
    public interface IIdentityProvider
    {
        ClaimsIdentity GetIdentity(string userName);
    }
}