﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Cosmopolitan.Services.Identity
{
    public class IdentityProvider : IIdentityProvider
    {
        public ClaimsIdentity GetIdentity(string userName)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
            };
            var claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}