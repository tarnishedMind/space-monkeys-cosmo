﻿using Cosmopolitan.Infrastracture.Services.Session;
using NHibernate;

namespace Cosmopolitan.Services.Session
{
    public class SessionProvider : ISessionProvider
    {
        private readonly ISessionFactory _sessionFactory;
        private ISession _session;

        public SessionProvider(WebSessionFactory webSessionFactory)
        {
            _sessionFactory = webSessionFactory.Init();
            _session = _sessionFactory.OpenSession();
        }

        public void OpenSession()
        {
            _session = _sessionFactory.OpenSession();
        }

        public ISession Session => _session;
        public void CloseSession()
        {
            _session.Close();
        }
    }
}