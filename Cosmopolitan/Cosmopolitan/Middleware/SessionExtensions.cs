﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Session;

namespace Cosmopolitan.Middleware
{
    public static class SessionExtensions
    {
        public static IApplicationBuilder UseHibernateSession(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<SessionMiddleware>();
        }
    }
}