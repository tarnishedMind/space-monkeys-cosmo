import { SortService } from './../../services/sort.service';
import { VkApiService } from './../../services/vk.api.service';
import { AppState } from './../index';
import { ThunkAction } from "redux-thunk";
import { AnyAction } from 'redux';
import { AxiosResponse } from 'axios';
import { setSearchPersons, SetPosts } from './actions';

const vkApiService = new VkApiService();

export const thunkSetPersonsBySearch = (name: string): ThunkAction<void, AppState, null, AnyAction> => async dispatch => {
    try {
        const res: AxiosResponse = await vkApiService.searchUser(name); 
        console.log('vkApiService.searchUser', res);
        dispatch(setSearchPersons(res.data.result.response.items));
    } catch (error) {
        console.log(error);
    }
}
export type thunkSetPersonsBySearch = typeof thunkSetPersonsBySearch;

export const thunkSetPosts = (ids: number[]): ThunkAction<void, AppState, null, AnyAction> => async dispatch => {
    try {
        const posts = await getPostsForManyUsers(ids);
        dispatch(SetPosts(posts));
    } catch (error) {
        console.log(error);
    }
}
export type thunkSetPosts = typeof thunkSetPosts;


export const getPostsForManyUsers = async (ids: number[]) => {
    let posts: any[] = [];
    for (const id of ids) {
        const res: AxiosResponse = await vkApiService.getWall(id);
        posts = [...posts, ...res.data.result.response.items];
    }
    posts.sort(SortService.sortByDateDescending);
    console.log(posts);
    return posts;
}


