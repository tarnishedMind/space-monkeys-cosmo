export interface PersonsState {
    searchPersons: any[], // need type
    targetPersons: any[], 
    posts: any[]
}

export const SET_SEARCH_PERSONS = "SET_SEARCH_PERSONS";
export const ADD_SUB_PERSON = "ADD_SUB_PERSON";
export const DEL_SUB_PERSON = "DEL_SUB_PERSON";
export const SET_SUB_PERSONS = "SET_SUB_PERSONS";

export const SET_POSTS = 'SET_POSTS';

// persons
interface SetSearchPersons {
    type: typeof SET_SEARCH_PERSONS;
    payload: any[] // need type
}

interface AddSubPerson {
    type: typeof ADD_SUB_PERSON;
    payload: any // need type
}

interface DelSubPerson {
    type: typeof DEL_SUB_PERSON;
    payload: any // need type
}

interface SetSubPersons {
    type: typeof SET_SUB_PERSONS;
    payload: any[] // need type
}

// posts
interface SetPosts {
    type: typeof SET_POSTS;
    payload: any[] // need type
}


export type PersonsActionTypes = SetSearchPersons | AddSubPerson | DelSubPerson | SetSubPersons
| SetPosts;