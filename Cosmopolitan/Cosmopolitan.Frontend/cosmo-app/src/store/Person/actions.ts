import { SET_SEARCH_PERSONS, ADD_SUB_PERSON, DEL_SUB_PERSON, SET_POSTS, SET_SUB_PERSONS } from './types';

export function setSearchPersons(list: any[]) { // need type 
    return {
        type: SET_SEARCH_PERSONS,
        payload: list
    }
}

export function addSubPerson(person: any) { // need type
    return {
        type: ADD_SUB_PERSON,
        payload: person 
    }
}

export function delSubPerson(person: any) { // need type
    return {
        type: DEL_SUB_PERSON,
        payload: person 
    }
}

export function SetSubPersons(persons: any[]) { // need type
    return {
        type: SET_SUB_PERSONS,
        payload: persons 
    }
}

// posts
export function SetPosts(posts: any[]) { // need type
    return {
        type: SET_POSTS,
        payload: posts 
    }
}
