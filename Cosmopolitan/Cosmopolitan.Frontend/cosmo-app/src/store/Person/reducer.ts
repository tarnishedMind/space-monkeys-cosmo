import { PersonsState, PersonsActionTypes, SET_SEARCH_PERSONS, ADD_SUB_PERSON, DEL_SUB_PERSON, SET_POSTS, SET_SUB_PERSONS } from "./types";

const initialState: PersonsState = {
    searchPersons: new Array<any>(), // need type
    targetPersons: new Array<any>(),
    posts: new Array<any>()
};

export function personsReducer(state = initialState, action: PersonsActionTypes): PersonsState {
    switch(action.type) {
        case SET_SEARCH_PERSONS:
            return {
                searchPersons: action.payload,
                targetPersons: state.targetPersons,
                posts: state.posts
            }
        case ADD_SUB_PERSON:            
            return {
                searchPersons: state.searchPersons,
                targetPersons: [...state.targetPersons, action.payload],
                posts: state.posts
            }
        case DEL_SUB_PERSON:          
            return {
                searchPersons: state.searchPersons,
                targetPersons: state.targetPersons.filter(person => person.id !== action.payload.id),
                posts: state.posts
            }
        case SET_SUB_PERSONS:          
            return {
                searchPersons: state.searchPersons,
                targetPersons: action.payload,
                posts: state.posts
            }
        case SET_POSTS:          
            return {
                searchPersons: state.searchPersons,
                targetPersons: state.targetPersons,
                posts: action.payload
            }
        default:
            return state;
    }
}