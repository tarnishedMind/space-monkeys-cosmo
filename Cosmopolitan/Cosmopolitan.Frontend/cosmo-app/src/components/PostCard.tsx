import React, { Fragment } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { Card, Icon, Avatar, Divider } from 'antd';
import { AppState } from '../store';
import ImageGallery from 'react-image-gallery';
const { Meta } = Card;

interface PostCardProps extends RouteComponentProps {
    showPost: Function,
    data: any, // post
    targetPersons: any[]
}

interface PostCardState {
}

class PostCard extends React.Component<PostCardProps, PostCardState> {

    getPersonFromStore = () => {
        const arr = this.props.targetPersons.filter(person => person.id === this.props.data.owner_id);
        return arr[0];
    }

    render() {
        const owner: any = this.getPersonFromStore();
        const postId = this.props.data.id;
        const postLink = `https://vk.com/id${owner.id}?w=wall${owner.id}_${postId}`;
        const publichedDate = new Date(this.props.data.date * 1000).toDateString();
        const publishedTime = new Date(this.props.data.date * 1000).toLocaleTimeString();

        const images = this.props.data.attachments && this.props.data.attachments.map((item: any) => {
            if (item.type === 'photo') { // audio, video
                return {
                    original: item.photo.photo_604 || item.photo.photo.photo_320,
                    thumbnail: item.photo.photo_130,
                }
            } else {
                return {
                    original: null,
                    thumbnail: null,
                }
            }
        });
        const ImageGalleryEl = images && images[0].original ? <ImageGallery items={images} /> : null;

        return (
            <Fragment>
                <Card
                    style={{ width: '50%', marginLeft: '25%' }}
                    actions={[
                        <a href={postLink}><Icon type="link" /></a>,
                        <Icon onClick={() => this.props.showPost()} type="info-circle" />,
                        <span>Views: {this.props.data.views.count}</span>,
                    ]}
                >
                    <Meta
                        avatar={<Avatar size={64} src={owner.photo_max} />}
                        title={`${owner.first_name} ${owner.last_name}`}
                        description={`${publichedDate} at ${publishedTime}`}
                    />
                    <Divider />
                    <p>{this.props.data.text}</p>
                    <div className="image-galery">
                        {ImageGalleryEl}
                    </div>                    
                </Card>
            </Fragment>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    targetPersons: state.personReducer.targetPersons
});
const connectedPostContainer = connect(mapStateToProps, {})(PostCard);
export default withRouter(connectedPostContainer);