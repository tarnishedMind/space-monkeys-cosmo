import React, { Fragment } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { Card, Avatar, Icon } from 'antd';
import { Person } from '../types/person';
import { addSubPerson, delSubPerson } from '../store/Person/actions';
import { FollowersService } from '../services/followers.service';
const { Meta } = Card;

interface SubscribeCardProps extends RouteComponentProps {
    data: Person,
    delSubPerson: typeof delSubPerson
}

interface SubscribeCardState {
}

class SubscribeCard extends React.Component<SubscribeCardProps, SubscribeCardState> {
    private http = new FollowersService();
    render() {
        const profileLink = `https://vk.com/id${this.props.data.id}`;
        return (
            <Fragment>
                <Card
                    hoverable
                    style={{ width: '95%' }}
                >
                    <Meta
                        avatar={<Avatar src={this.props.data.photo_max} />}
                        title={`${this.props.data.first_name} ${this.props.data.last_name}`}
                    // description="This is the description"
                    />
                    <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                        <a href={profileLink}>link</a>
                        <Icon onClick={async () => { 
                            this.props.delSubPerson(this.props.data);
                            await this.http.delFollower(this.props.data.subGuid as string);
                            }} type="user-delete" />
                    </div>
                </Card>
            </Fragment>
        );
    }
}

export default withRouter(connect(null, { delSubPerson })(SubscribeCard));