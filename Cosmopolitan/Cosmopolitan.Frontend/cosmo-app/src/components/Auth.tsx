import React from 'react';
import { Form, Icon, Input, Button, message } from 'antd';
import { RouteComponentProps, withRouter } from 'react-router';
import { AuthService } from '../services/auth.service';
import { TokenService } from '../services/token.service';
import { AxiosResponse } from 'axios';

interface NormalAuthFormProps extends RouteComponentProps {
    form: any,
    type: string // login or register
}

class NormalAuthForm extends React.Component<NormalAuthFormProps> {

    private http = new AuthService();

    handleSubmit = (e: any) => {
        e.preventDefault();
        this.props.form.validateFields(async (err: any, values: any) => {
            if (!err) {
                console.log('Received values of form: ', values);
                if (this.props.type === 'login') {
                    let res: AxiosResponse = {} as AxiosResponse;
                    try {
                        res = await this.http.login(values);
                        console.log('login res', res);
                        TokenService.setAccessToken(res.data.result.access_token);
                        // console.log(TokenService.getBearerToken());
                        this.props.history.push('/sample');
                    } catch (error) {
                        message.error(res.data.error.message);
                        console.log(error);
                    }
                } else {
                    const res: AxiosResponse = await this.http.register(values);
                    res.data.success && this.props.history.push('/login');
                    !res.data.success && message.error(res.data.error.message);;
                }
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        const btnText = this.props.type === 'login' ? 'Log in' : 'Join us';
        const header = this.props.type === 'login' ? <h1>Login</h1> : <h1>Register</h1>;
        const joinLink = this.props.type === 'login' ? <a onClick={() => this.props.history.push('/register')}>Or register now!</a>
            : <a onClick={() => this.props.history.push('/login')}>Or login if you are already have account!</a>;
        return (
            <div className="auth">
                {header}
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                        {getFieldDecorator('Login', {
                            rules: [{ required: true, message: 'Please input your username!' }],
                        })(
                            <Input
                                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder="Username"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('Password', {
                            rules: [{ required: true, message: 'Please input your Password!' }],
                        })(
                            <Input
                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                type="password"
                                placeholder="Password"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            {btnText}
                        </Button>
                        <br />
                        {joinLink}
                    </Form.Item>
                </Form>
            </div>
        );
    }
}

const WrappedNormalAuthForm = Form.create({ name: 'normal_login' })(NormalAuthForm) as any;
export default withRouter(WrappedNormalAuthForm) as any;