import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import PersonCard from './PersonCard';
import SubscribeCard from './SubscribeCard';
import { FollowersService } from '../services/followers.service';
import { Input, Button, Icon, notification } from 'antd';
import { AppState } from '../store';
import { thunkSetPersonsBySearch, thunkSetPosts } from '../store/Person/thunks';
import { connect } from 'react-redux';
import { Person } from '../types/person';
import { AxiosResponse } from 'axios';
import { VkApiService } from '../services/vk.api.service';
import { TokenService } from '../services/token.service';
import { SetSubPersons } from '../store/Person/actions';
const { Search } = Input;

interface SampleProps extends RouteComponentProps {
    searchPersons: any[],
    targetPersons: any[],
    SetSubPersons: typeof SetSubPersons,
    thunkSetPersonsBySearch: thunkSetPersonsBySearch,
    thunkSetPosts: thunkSetPosts
}

class Sample extends React.Component<SampleProps> {

    private followersService = new FollowersService();
    private vkApiService = new VkApiService();

    constructor(props: SampleProps){
        super(props);
        if (TokenService.getAccessToken() == null) {
            this.props.history.push('login');
        }
    }

    async componentDidMount() {        
        this.props.thunkSetPersonsBySearch('');
        this.fillSubscription();
    }

    fillSubscription = async () => {
        let arrTargetUser = [];
        const res: AxiosResponse = await this.followersService.getFollowers();
        console.log('getFollowers', res);

        if (res.data.result && res.data.result.length) {
            for (let user of res.data.result) {
                const targetUser: AxiosResponse = await this.vkApiService.getUser(user.idUser);
                arrTargetUser.push(Object.assign(targetUser.data.result.response[0], { subGuid: user.id }));
            }   
            console.log(arrTargetUser);  
            this.props.SetSubPersons(arrTargetUser); 
        } else {
            notification['info']({
                message: 'Info Notification',
                description:
                  'Your subscription list empty yet.',
              });
        }
          
    }
    

    goToPosts = () => {
        const ids = this.props.targetPersons.map(person => person.id);        
        this.props.thunkSetPosts(ids);
        this.props.history.push('/posts');
    }

    render() {
        console.log('state check', this.props);
        const persons = this.props.searchPersons.map(person => <PersonCard key={person.id} data={person} />);
        const targetPersons = this.props.targetPersons.map(person => <SubscribeCard key={person.id} data={person} />);
        
        return (
            <div>
                <Search style={{ marginTop: '15px' }} placeholder="input search text" size="large"
                    onSearch={value => this.props.thunkSetPersonsBySearch(value)}
                    addonBefore={<Icon onClick={() => this.props.thunkSetPersonsBySearch('')} type="team" />}
                    enterButton />
                <div style={{ display: 'flex' }}>
                    <div className="search-persons">
                        {persons}
                    </div>
                    <div className="subs-persons">
                        <Button className="show-btn" onClick={this.goToPosts} type="primary" shape="round" icon="search" size="large">Show posts</Button>
                        {targetPersons}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    searchPersons: state.personReducer.searchPersons,
    targetPersons: state.personReducer.targetPersons,
    SetSubPersons: SetSubPersons,
    thunkSetPersonsBySearch: thunkSetPersonsBySearch,
    thunkSetPosts: thunkSetPosts
});
const connectedSample = connect(mapStateToProps, { thunkSetPersonsBySearch, thunkSetPosts, SetSubPersons })(Sample);
export default withRouter(connectedSample);