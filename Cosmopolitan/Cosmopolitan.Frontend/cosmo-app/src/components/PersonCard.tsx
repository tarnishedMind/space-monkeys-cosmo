import React, { Fragment } from 'react';
import { Person } from '../types/person';
import { Card, Checkbox, Icon, Avatar, Tooltip } from 'antd';
import { addSubPerson, delSubPerson, SetSubPersons } from '../store/Person/actions';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { AppState } from '../store';
import { FollowersService } from '../services/followers.service';
import { AxiosResponse } from 'axios';
const { Meta } = Card;

interface PersonCardProps extends RouteComponentProps {
    data: Person,
    targetPersons: any[],
    addSubPerson: typeof addSubPerson,
    delSubPerson: typeof delSubPerson,
    SetSubPersons: typeof SetSubPersons
}

class PersonCard extends React.Component<PersonCardProps> {

    state = { isChecked: false };

    private followersService = new FollowersService();
    private guid = '';

    onChange = (e: any) => {
        console.log(`checked = ${e.target.checked}`);
        this.setState({ isChecked: e.target.checked });
        if (e.target.checked) {
            this.add();
        } else {
            this.del();
        }
    }

    add = async () => {
        const res: AxiosResponse = await this.followersService.addFollower({ followers: { IdUser: this.props.data.id } });
        this.props.addSubPerson(Object.assign(this.props.data, { subGuid: res.data.result }));
        this.guid = res.data.result;
    }

    del = async () => {
        this.props.delSubPerson(this.props.data);
        console.log(this.props.data.subGuid);
        await this.followersService.delFollower(this.props.data.subGuid as string);
    }

    render() {
        const wasFound = this.props.targetPersons.filter(person => this.props.data.id === person.id);
        // wasFound.length > 0 && this.setState({ isChecked: true });

        const accept: any = this.props.data.verified === 1 ? <Icon type="safety" /> : <Icon type="issues-close" />;
        const profileLink = `https://vk.com/id${this.props.data.id}`;
        return (
            <Fragment>
                <Card
                    // hoverable
                    style={{ width: '100%' }}
                    actions={[
                        <Tooltip title="Accepted profile">
                            {accept}
                        </Tooltip>,
                        <Tooltip title="Link to page">
                        <a href={profileLink}><Icon type="link" /></a>
                        </Tooltip>,                        
                        !(wasFound.length > 0) && <Tooltip title="Add to sub list"><Icon onClick={this.add} type="user-add" /></Tooltip>
                        // <Checkbox onChange={this.onChange} checked={this.state.isChecked}>Subscribe</Checkbox>                     
                    ]}
                >
                    <Meta title={`${this.props.data.first_name} ${this.props.data.last_name}`} description="ВКонтакте"
                        avatar={<Avatar src={this.props.data.photo_max} size={128} />}
                    />
                </Card>
            </Fragment>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    targetPersons: state.personReducer.targetPersons
});
export default withRouter(connect(mapStateToProps, { addSubPerson, delSubPerson, SetSubPersons })(PersonCard));