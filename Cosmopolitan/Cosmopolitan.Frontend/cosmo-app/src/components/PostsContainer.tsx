import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Modal, Button, Icon } from 'antd';
import { AppState } from '../store';
import { connect } from 'react-redux';
import PostCard from './PostCard';
import { SortService } from '../services/sort.service';
import { SetPosts } from '../store/Person/actions';
import { AxiosResponse } from 'axios';
import { FollowersService } from '../services/followers.service';
import { VkApiService } from '../services/vk.api.service';
import { getPostsForManyUsers } from '../store/Person/thunks';

interface PostContainerProps extends RouteComponentProps {
    posts: any[],
    SetPosts: typeof SetPosts,
    targetPersons: any[]
}

interface PostContainerState {
    visible: boolean,
    postContent: any,
    sort: number
}

class PostContainer extends React.Component<PostContainerProps, PostContainerState> {

    private followersService = new FollowersService();
    private vkApiService = new VkApiService();

    state = { visible: false, postContent: '', sort: 0 };

    showModal = (content: any) => {
        this.setState({
            visible: true,
            postContent: content
        });
    };

    handleOk = (e: any) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = (e: any) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    async componentDidMount() {
        if (!this.props.targetPersons.length) {
            this.props.history.push('/sample');
            // const res: AxiosResponse = await this.followersService.getFollowers();
            // console.log('getFollowers', res);  
            // const ids = res.data.result.map((folUser: any) => folUser.idUser);
            // const posts = await getPostsForManyUsers(ids);
            // this.props.SetPosts(posts);            
            // need fill targetPerson
        }
    }

    dateSort = () => {
        this.state.sort === 0 ? this.setState({ sort: 1 }) : this.setState({ sort: 0 });
        this.sort();
    }
    viewsSort = () => {
        this.state.sort === 2 ? this.setState({ sort: 3 }) : this.setState({ sort: 2 });
        this.sort();
    }

    sort = () => {
        let postsCopy = [];
        postsCopy = [...this.props.posts];
        const func = SortService.getSortType(this.state.sort);
        postsCopy.sort(func);
        this.props.SetPosts(postsCopy);
    }


    render() {
        // let portal: any = document.createElement('portal');
        // portal.src = 'https://en.wikipedia.org/wiki/World_Wide_Web';
        // portal.style = '...';
        // document.body.appendChild(portal);
        // portal.activate();

        console.log('PostContainer state check', this.props);
        const posts = this.props.posts.map(post => <PostCard data={post} key={post.id} showPost={() => { this.showModal(post.text) }} />);
        return (
            <div>
                <div style={{ textAlign: 'center' }}>
                    <Button.Group size={'large'}>
                        <Button onClick={() => { this.props.history.push('/sample') }} type="primary">
                            <Icon type="left" />
                            Back
          </Button>
                        <Button onClick={this.dateSort} type="primary">
                            Sort by date
                        </Button>
                        <Button onClick={this.viewsSort} type="primary">
                            Sort by views
                        </Button>
                    </Button.Group>
                </div>
                <div className="post-container">
                    {posts}
                    <Modal
                        title="Additional info"
                        width='800px'
                        visible={this.state.visible}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                    >
                        Python analysis will be here...
                        {/* <p>{this.state.postContent}</p> */}
                    </Modal>
                </div>
            </div>

        );
    }
}

const mapStateToProps = (state: AppState) => ({
    posts: state.personReducer.posts,
    targetPersons: state.personReducer.targetPersons
});
const connectedPostContainer = connect(mapStateToProps, { SetPosts })(PostContainer);
export default withRouter(connectedPostContainer);