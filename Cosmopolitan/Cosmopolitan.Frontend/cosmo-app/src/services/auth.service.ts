import Axios from "axios";

export class AuthService {
    public login(user: {Login: string, Password: string}) {
        // const url = 'http://localhost:5000/account/login';
        const url = 'http://smirnoff411-001-site1.itempurl.com/account/login';
        return Axios.post(url, user);
    }

    public register(user: {Login: string, Password: string}) {
        // const url = 'http://localhost:5000/account/register';
        const url = 'http://smirnoff411-001-site1.itempurl.com/account/register';
        return Axios.post(url, user);
    }
}