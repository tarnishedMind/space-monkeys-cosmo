export class SortService {
    public static sortByDateAscending (a: any, b: any) {
        if (a.date < b.date) return -1;
        if (a.date > b.date) return 1;
        return 0;
    }
    
    public static sortByDateDescending (a: any, b: any) {
        if (a.date < b.date) return 1;
        if (a.date > b.date) return -1;
        return 0;
    }

    public static sortByViewsAscending (a: any, b: any) {
        if (a.views.count < b.views.count) return -1;
        if (a.views.count > b.views.count) return 1;
        return 0;
    }

    public static sortByViewsDescending (a: any, b: any) {
        if (a.views.count < b.views.count) return 1;
        if (a.views.count > b.views.count) return -1;
        return 0;
    }

    public static getSortType(value: number) {
        switch (value) {
            case 0:
                return SortService.sortByDateAscending;
            case 1:
                return SortService.sortByDateDescending;
            case 2:
                return SortService.sortByViewsAscending;
            case 3:
                return SortService.sortByViewsDescending;
            default:
                break;
        }
    }
}