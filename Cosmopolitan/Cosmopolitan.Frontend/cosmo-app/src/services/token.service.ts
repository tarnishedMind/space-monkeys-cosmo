export class TokenService {
    
    public static setAccessToken(token: string) {
        localStorage.setItem('access_token', token);
    } 

    public static getAccessToken(): string | null {
        return localStorage.getItem('access_token');
    } 

    public static getBearerToken(): string {
        const token = localStorage.getItem('access_token');
        return token ? `Bearer ${token}` : ``;
    } 
    
}
