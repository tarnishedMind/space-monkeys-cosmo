import Axios from "axios";
import { TokenService } from "./token.service";

export class FollowersService {
    
    public addFollower(followers: { followers: { IdUser: number}}) {
        const url = 'http://smirnoff411-001-site1.itempurl.com/user/addfollower';
        return Axios.post(url, followers, {
            headers: {
                'Authorization': TokenService.getBearerToken()
            }
        });
    }

    public delFollower(guid: string) {
        const url = 'http://smirnoff411-001-site1.itempurl.com/user/deletefollower/' + guid.toString();
        return Axios.delete(url, {
            headers: {
                'Authorization': TokenService.getBearerToken()
            }
        });
    }

    public getFollowers() {
        const url = 'http://smirnoff411-001-site1.itempurl.com/user/getfollowers';
        return Axios.get(url, {
            headers:
            {
                'Authorization': TokenService.getBearerToken()
            }
        });
    }
}