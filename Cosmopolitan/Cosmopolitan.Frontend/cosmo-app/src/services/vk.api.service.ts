import Axios from "axios";

export class VkApiService {

    public searchUser(name: string) {
        const url = 'http://smirnoff411-001-site1.itempurl.com/vkapi/users/item';

        const params = {
            q: name,
            country: 1,
            fields: 'verified,photo_max'
        }

        return Axios.get(url, { params: params });
    }

    public getWall(id: number) {
        const url = 'http://smirnoff411-001-site1.itempurl.com/vkapi/wall/item';

        const params = {
            ownerId: id.toString(),
            filter: 'owner'
        }

        return Axios.get(url, { params: params });
    }

    public getUser(id: number) {
        const url = 'http://smirnoff411-001-site1.itempurl.com/vkapi/user/item';

        const params = {
            userIds: id,
            fields: 'verified,photo_max,city'
        }

        return Axios.get(url, { params: params });
    }

}