import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './App.scss';
import { Row, Col } from 'antd';
import Sample from './components/Sample';
import PostsContainer from './components/PostsContainer';
import WrappedNormalAuthForm from './components/Auth';

class App extends React.Component<any> {
  // componentDidMount() {
  //   this.props.history.push('/sample');
  // }
  render() {

    const router =
      <Router>
        <Switch>
          <Route path="/login" component={() => <WrappedNormalAuthForm type='login' />} />
          <Route path="/register" component={() => <WrappedNormalAuthForm type='register' />} />
          <Route path="/sample" component={Sample} />
          <Route path="/posts" component={PostsContainer} />
        </Switch>
      </Router>;

    return (
      <div>
        <Row>
          <Col span={16} offset={4}>
            {/* Top */}
          </Col>
        </Row>
        <Row>
          <Col span={16} offset={4}>
            {router}
          </Col>
        </Row>
        <Row>
          <Col span={16} offset={4}>
            {/* Down */}
          </Col>
        </Row>
      </div>
    );
  }
}

export default App;
