export interface Person {
    id: number,
    first_name: string,
    last_name: string,
    track_code: string,
    verified: number,
    photo_max: string,
    subGuid?: string
}