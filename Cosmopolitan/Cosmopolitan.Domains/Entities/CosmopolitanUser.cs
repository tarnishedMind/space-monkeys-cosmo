﻿using System;
using System.Collections.Generic;
using Cosmopolitan.Domains.Entities.Base;

namespace Cosmopolitan.Domains.Entities
{
    public class CosmopolitanUser : Entity
    {
        public virtual string Login { get; set; }
        public virtual string PasswordHash { get; set; }
        public virtual ICollection<Follower> IdPosts { get; set; }
    }
}