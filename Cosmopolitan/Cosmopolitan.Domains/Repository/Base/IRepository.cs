﻿using System;
using System.Linq;
using Cosmopolitan.Domains.Entities.Base;

namespace Cosmopolitan.Domains.Repository.Base
{
    public interface IRepository<T> where T : Entity
    {
        void Delete(T entity);
        T GetById(Guid id);
        IQueryable<T> List();
        void Save(T entity);
    }
}