﻿using System.Collections.Generic;

namespace Cosmopolitan.Contacts.Dto.Vk
{
    public class VkWallUserDto
    {
        public VkWallUserResponseDto response { get; set; }
    }

    public class VkWallUserResponseDto
    {
        public int count { get; set; }
        public List<VkWallUserItemDto> items { get; set; }
    }

    public class VkWallUserItemDto
    {
        public int id { get; set; }
        public int from_id { get; set; }
        public int owner_id { get; set; }
        public int date { get; set; }
        public string post_type { get; set; }
        public string text { get; set; }
        public List<Attachment> attachments { get; set; }
        public List<CopyHistory> copy_history { get; set; }
        public PostSource2 post_source { get; set; }
        public Comments comments { get; set; }
        public Likes likes { get; set; }
        public Reposts reposts { get; set; }
        public Views views { get; set; }
    }

    public class Video
    {
        public int id { get; set; }
        public int owner_id { get; set; }
        public string title { get; set; }
        public int duration { get; set; }
        public string description { get; set; }
        public int date { get; set; }
        public int comments { get; set; }
        public int views { get; set; }
        public int local_views { get; set; }
        public string photo_130 { get; set; }
        public string photo_320 { get; set; }
        public string photo_640 { get; set; }
        public string photo_800 { get; set; }
        public string access_key { get; set; }
        public string platform { get; set; }
        public int can_add { get; set; }
        public string track_code { get; set; }
    }

    public class Photo
    {

        public int id { get; set; }
        public int album_id { get; set; }
        public int owner_id { get; set; }
        public string text { get; set; }
        public string date { get; set; }
        public string access_key { get; set; }
        public List<Size> sizes { get; set; }
        public string photo_75 { get; set; }
        public string photo_130 { get; set; }
        public string photo_604 { get; set; }
        public string photo_807 { get; set; }
        public string photo_1280 { get; set; }
        public string photo_2560 { get; set; }
    }

    public class Size
    {
        public string type { get; set; }
        public string url { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Attachment
    {
        public string type { get; set; }
        public Video video { get; set; }
        public Photo Photo { get; set; }
    }

    public class PostSource
    {
        public string type { get; set; }
    }

    public class CopyHistory
    {
        public int id { get; set; }
        public int owner_id { get; set; }
        public int from_id { get; set; }
        public int date { get; set; }
        public string post_type { get; set; }
        public string text { get; set; }
        public List<Attachment> attachments { get; set; }
        public PostSource post_source { get; set; }
    }

    public class PostSource2
    {
        public string type { get; set; }
    }

    public class Comments
    {
        public int count { get; set; }
        public int can_post { get; set; }
        public bool groups_can_post { get; set; }
    }

    public class Likes
    {
        public int count { get; set; }
        public int user_likes { get; set; }
        public int can_like { get; set; }
        public int can_publish { get; set; }
    }

    public class Reposts
    {
        public int count { get; set; }
        public int user_reposted { get; set; }
    }

    public class Views
    {
        public int count { get; set; }
    }
}