﻿using System.Collections.Generic;

namespace Cosmopolitan.Contacts.Dto.Vk
{
    public class VkUserDto
    {
        public VkApiUserResponseDto response { get; set; }
    }

    public class VkApiUserResponseDto
    {
        public int count { get; set; }
        public List<VkApiUserItemDto> items { get; set; }
    }

    public class VkApiUserItemDto
    {

        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string track_code { get; set; }
        public int verified { get; set; }
        public string photo_max { get; set; }
    }
}