﻿namespace Cosmopolitan.Contacts.Dto.Account
{
    public class TokenDto
    {
        public string access_token { get; set; }
    }
}