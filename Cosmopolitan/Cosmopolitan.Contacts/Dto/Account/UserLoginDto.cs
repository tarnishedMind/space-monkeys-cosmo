﻿namespace Cosmopolitan.Contacts.Dto.Account
{
    public class UserLoginDto
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}