﻿using System.Collections;
using System.Collections.Generic;

namespace Cosmopolitan.Contacts.Dto.User
{
    public class AddFollowerDto
    {
        public IEnumerable<FollowerDto> followers { get; set; }
    }
}